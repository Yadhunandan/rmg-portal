function viewMore() {
    $('.tbl-cntr tbody tr td span.view-more').on('click', function () {
        if ($(window).width() < 900) {
            $(this).parents('tbody').find('tr').each(function (index, item) {
                if ($(item).hasClass('more')) {
                    $(item).removeClass('more')
                }
            });
            $(this).parents('tr').addClass('more')
            $('html, body').stop().animate({ scrollTop: $(this).parents('tr').offset().top - 75 }, 200, 'swing');
        }
    })
}

function rowEdit() {
    $('table tbody td span.edit').on('click', function () {
        $(this).hide();
        $(this).parents('td').addClass('expnd');
        $(this).siblings('.update, .cancel').show(300);
        $(this).parents('tr').find('textarea, input').each(function (index, item) {
            $(item).attr('disabled', false);
        })
        $(this).parents('tr').find('select').each(function (index, item) {
            $(item).attr('disabled', false);
            $(item).removeClass('hide');
            $(item).siblings('label').addClass('hide');
        })
    })
}

var popupInfo;

function rowUpdate() {
    $('table tbody td span.update').on('click', function () {
        if (!$(this).hasClass('approve')) {
            if (!$(this).hasClass('popup')) {
                $(this).hide();
                $(this).siblings('.edit').show();
                $(this).siblings('.cancel').hide();
                $(this).parents('td').removeClass('expnd');
                $(this).parents('tr').find('textarea, input').each(function (index, item) {
                    $(item).attr('disabled', true);
                })
                $(this).parents('tr').find('select').each(function (index, item) {
                    $(item).attr('disabled', true);
                    $(item).addClass('hide');
                    $(item).siblings('label').removeClass('hide');
                })
            } else {
                $('.gry-bgnd').addClass('show');
                popupInfo = $(this);
                return false;
            }
        } else {
            $('.gry-bgnd').addClass('show');
        }
    })

}

function rowCancel() {
    $('table tbody td span.cancel').on('click', function () {
        if (!$(this).hasClass('approve')) {
            $(this).hide();
            $(this).siblings('.edit').show();
            $(this).siblings('.update').hide();
            $(this).parents('td').removeClass('expnd');
            $(this).parents('tr').find('textarea, input').each(function (index, item) {
                $(item).attr('disabled', true);
            })
            $(this).parents('tr').find('select').each(function (index, item) {
                $(item).attr('disabled', true);
                $(item).addClass('hide');
                $(item).siblings('label').removeClass('hide');
            })
        }
    })

}

function hideCalander() {
    $('.datepicker').on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
}


$(document).on('all.bs.table', '.resource-search-landing #resource-search-table', function () {
    viewMore();
});



$(document).on('all.bs.table', '.open-requisition-landing #requisition-view , .open-requisition-landing #tech-stack , .open-requisition-landing #bu-stack', function () {
    rowEdit();
    rowUpdate();
    rowCancel();
    viewMore();
    openNoReqClick();
});


$(document).on('all.bs.table', '.close-requisition-landing #requisition-view , .close-requisition-landing #tech-stack , .close-requisition-landing #bu-stack', function () {
    viewMore();
    closeNoReqClick();
});


$(document).on('all.bs.table', '.code-details-landing .search-result-section #search-result-table', function () {
    viewMore();
});


$(document).on('all.bs.table', '.code-details-landing .details-section .proposed-resources-section .tbl-cntr #proposed-resource-table', function () {
    viewMore();
    rowEdit();
    rowUpdate();
    rowCancel();
});


$(document).on('all.bs.table', '.proposed-resources-landing .tbl-cntr #proposed-resource-table', function (a, b) {
    rowEdit();
    rowUpdate();
    rowCancel();
    viewMore();
});


$(document).on('all.bs.table', '.view-joiners-pipeline-landing #requisition-view , .view-joiners-pipeline-landing #tech-stack , .view-joiners-pipeline-landing #bu-stack ', function () {
    viewMore();
    viewJoinersNoReqClick();
});

$(document).on('all.bs.table', '.ceg-main-landing #identified-resources , .ceg-main-landing #available-roaster , .ceg-main-landing #resigned-exited-cases , .ceg-main-landing #overall-deployment', function (a, b) {
    rowEdit();
    rowUpdate();
    rowCancel();
    viewMore();
});


$(document).on('all.bs.table', '.approve-page-landing .tbl-cntr #re-provisional-allocation , .approve-page-landing .tbl-cntr #external-hiring', function () {
    rowEdit();
    rowUpdate();
    rowCancel();
    viewMore();
});

function viewJoinersNoReqClick() {
    $('.view-joiners-pipeline-landing #tech-stack tbody tr td a, .view-joiners-pipeline-landing #bu-stack tbody tr td a').on('click', function () {
        if ($(this).parents('td').hasClass('req-no')) {

            $('.view-joiners-pipeline-landing .navbar ul.navbar-nav li').each(function (index, item) {

                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
                if ($(item).attr('data-name') == 'requisition-view') {
                    $(item).addClass('active');
                }
            })

            $('.view-joiners-pipeline-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            });
            $('.view-joiners-pipeline-landing .tbl-cntr').find('#requisition-view').parents('.individual-tbl').addClass('active');

            $('html, body').animate({ scrollTop: 0 }, 0);
        }

    })
}


function openNoReqClick() {
    $('.open-requisition-landing #tech-stack tbody tr td a, .open-requisition-landing #bu-stack tbody tr td a').on('click', function () {
        if ($(this).parents('td').hasClass('req-no')) {
            $('.open-requisition-landing .navbar ul.navbar-nav li').each(function (index, item) {

                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
                if ($(item).attr('data-name') == 'requisition-view') {
                    $(item).addClass('active');
                }
            })

            $('.open-requisition-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            });
            $('.open-requisition-landing .tbl-cntr').find('#requisition-view').parents('.individual-tbl').addClass('active');

            $('html, body').animate({ scrollTop: 0 }, 0);
        }

    })
}


function closeNoReqClick() {
    $('.close-requisition-landing #tech-stack tbody tr td a, .close-requisition-landing #bu-stack tbody tr td a').on('click', function () {
        if ($(this).parents('td').hasClass('req-no')) {

            $('.close-requisition-landing .navbar ul.navbar-nav li').each(function (index, item) {

                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
                if ($(item).attr('data-name') == 'requisition-view') {
                    $(item).addClass('active');
                }
            })

            $('.close-requisition-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            });
            $('.close-requisition-landing .tbl-cntr').find('#requisition-view').parents('.individual-tbl').addClass('active');

            $('html, body').animate({ scrollTop: 0 }, 0);
        }

    })
}

$(document).on('change', '.table tbody tr td select', function(){
    $(this).siblings('label').empty();
    if($(this).val().toLowerCase() != 'select') {
        $(this).siblings('label').text($(this).val());
    }
})

$('[data-toggle="tooltip"]').tooltip();

$(document).ready(function () {

    // header js code starts

    $('.landing-page .cntr-cont .main-optn .app-logo').on('click', function () {
        let info = $(this).attr('data-info');
        $(this).siblings('.app-logo').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        })
        $(this).addClass('active');

        $('.landing-page .cntr-cont').children('.sub.options').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        });
        $('.landing-page .cntr-cont').children('.sub.options').each(function (index, item) {
            if ($(item).attr('data-info') == info) {
                $(item).addClass('active');
            }
        });

        // $(this).siblings('.options').addClass('show');
    })

    $('header .options .profile-pic .outer-pp').on('click', function () {
        $(this).siblings('.logout-popup').fadeToggle();
        $(this).parents('.profile-pic').toggleClass('active');
    })

    $('header .options .profile-pic .logout-popup .cls-btn').on('click', function () {
        $(this).parents('.profile-pic').removeClass('active');
        $(this).parents('.logout-popup').fadeOut();
    })


    $(document).mouseup(function (e) {
        var container = $("header .options .profile-pic .logout-popup");
        if (!$('header .options .profile-pic img').is(e.target) && !$('header .options .profile-pic').is(e.target) && !container.is(e.target) && container.has(e.target).length === 0) {
            container.parents('.profile-pic').removeClass('active');
            container.fadeOut();
        }
    });


    // header js code ends 

    // Resources Search Page JS Starts

    // date picker code

    $('.resource-search-landing .datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    hideCalander()
    //Form Submit code

    $('.resource-search-landing .submit').on('click', function () {

        $('.validate-error').html('');
        var formValidationStatus = 0;
        if ($('#primary-skill').val() == null) {
            formValidationStatus = 1;
            $('#primary-skill').siblings('.validate-error').html("Please enter Primary Skill");
        }

        if ($('#secondary-skill').val() == null) {
            formValidationStatus = 1;
            $('#secondary-skill').siblings('.validate-error').html("Please enter Secondary Skill");
        }

        if (formValidationStatus == 0) {
            return false
        } else {
            // submit the form
        }
    });

    $('.resource-search-landing .form-cls-btn ').on('click', function () {
        $(this).toggleClass('show');
        $('.resource-search-landing .form-blk').toggleClass('show');
        $('.validate-error').html('');
    })

    //multiselect and autosearch initialization

    $(".resource-search-landing #primary-skill, .resource-search-landing #secondary-skill").chosen({
        disable_search_threshold: 10,
        no_results_text: "Oops, nothing found!"
    });

    //resource page table initialization

    $('.resource-search-landing #resource-search-table').bootstrapTable({
        pagination: true,
        showColumns: true,
        showColumnsToggleAll: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    //selectpicker dropdown hide

    $('.selectpicker').on('changed.bs.select', function () {
        if ($(this).siblings('.dropdown-menu').hasClass('show')) {
            $(this).siblings('.dropdown-menu').removeClass('show')
        }
    })

    $('.resource-search-landing #resource-search-table tbody tr td span.data-info a.clickable').on('click', function () {
        $('.gry-bgnd').addClass('show');
    })

    // Resources Search Page JS Ends




    // Open requisition Page JS Starts

    //Table initialization

    $('.open-requisition-landing #tech-stack').bootstrapTable({
        pagination: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.open-requisition-landing #bu-stack').bootstrapTable({
        pagination: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.open-requisition-landing #requisition-view').bootstrapTable({
        pagination: true,
        sortable: true,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    //nav tab click and active

    $('.open-requisition-landing .navbar ul.navbar-nav li').on('click', function () {
        if ($(this).attr('data-name') != 'requisition-view') {
            $(this).parents('ul.nav').children('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            })
            $(this).addClass('active');
            $('.open-requisition-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            });
            $('.open-requisition-landing .tbl-cntr').find('#' + $(this).attr('data-name')).parents('.individual-tbl').addClass('active');
        }
    })

    // on click req number go to req view

    openNoReqClick();

    // Open requisition Page JS Ends


    // Close requisition Page JS Starts

    //Table initialization

    $('.close-requisition-landing #tech-stack').bootstrapTable({
        pagination: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.close-requisition-landing #bu-stack').bootstrapTable({
        pagination: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.close-requisition-landing #requisition-view').bootstrapTable({
        pagination: true,
        sortable: true,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });


    // date picker code

    $('.close-requisition-landing .datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    //nav tab click and active

    $('.close-requisition-landing .navbar ul.navbar-nav li').on('click', function () {
        if ($(this).attr('data-name') != 'requisition-view') {
            $(this).parents('ul.nav').children('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            })
            $(this).addClass('active');
            $('.close-requisition-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            });
            $('.close-requisition-landing .tbl-cntr').find('#' + $(this).attr('data-name')).parents('.individual-tbl').addClass('active');
        }
    })

    // on click req number go to req view

    closeNoReqClick();

    // Close requisition Page JS Ends


    // Code details Page JS Starts

    function codeDetailsFocusOnOptions() {
        if ($(window).width() < 700) {
            $('.code-details-landing .options ul').find('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $('.code-details-landing .options ul').animate({ scrollLeft: ($(item).width() * index) }, 500);
                }
            })
        }
    }

    $('.code-details-landing .options ul li a').on('click', function () {
        let current = $(this).parent('li');
        $(this).parents('ul').children('li').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        });
        $(this).parent('li').addClass('active');
        codeDetailsFocusOnOptions()
        $('.code-details-landing .details-section .child-details').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }

            if ($(item).attr('data-name') == $(current).attr('data-name')) {
                $(item).addClass('active');
            }
        });
    })

    codeDetailsFocusOnOptions()

    $('.code-details-landing .search-result-section #search-result-table').bootstrapTable({
        pagination: true,
        sortable: true,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });


    $('.code-details-landing .search-result-section .navbar ul.navbar-nav li').on('click', function () {
        $(this).parents('ul.nav').children('li').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        })
        $(this).addClass('active');
    })



    $(".code-details-landing .details-section .search-result-section .tbl-cntr table tbody tr td input").on('change', function () {
        if (this.checked) {
            if (!$('.code-details-landing .details-section .search-result-section .sbmt-btns').hasClass('show')) {
                $('.code-details-landing .details-section .search-result-section .sbmt-btns').addClass('show');
                $('.code-details-landing .details-section .search-result-section .sbmt-btns').find('button').each(function (index, item) {
                    $(item).attr('disabled', false);
                })
            }
        } else {
            let noneChecked = 0;
            $(this).parents('tbody').find('input').each(function (index, item) {
                if (item.checked) {
                    noneChecked = 1;
                }
            })
            if (noneChecked == 0) {
                $('.code-details-landing .details-section .search-result-section .sbmt-btns').removeClass('show');
                $('.code-details-landing .details-section .search-result-section .sbmt-btns').find('button').each(function (index, item) {
                    $(item).attr('disabled', true);
                })
            }
        }
    });


    $('.code-details-landing .details-section .proposed-resources-section .tbl-cntr #proposed-resource-table').bootstrapTable({
        pagination: true,
        sortable: true,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    var startDate = '-5d';

    function setStartDate() {
        var datepickerStartDate = new Date();
        var firstDay = new Date(datepickerStartDate.getFullYear(), datepickerStartDate.getMonth(), 1);
        if (datepickerStartDate.getDate() - firstDay.getDate() < 5) {
            startDate = '-' + datepickerStartDate.getDate() - firstDay.getDate() + 'd';
        }
    }
    setStartDate()

    $('.code-details-landing .details-section .proposed-resources-section .tbl-cntr #proposed-resource-table tr td .datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: startDate,
        endDate: '+0d'
    });


    $('.code-details-landing .search-result-section #search-result-table tbody tr td span.data-info a.clickable').on('click', function () {
        $('.gry-bgnd').addClass('show');
    })

    // Code details Page JS Ends


    // Proposed Resources Page JS Starts

    $('.proposed-resources-landing .tbl-cntr #proposed-resource-table').bootstrapTable({
        pagination: true,
        sortable: false,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.proposed-resources-landing #proposed-resource-table tr td .datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    hideCalander();

    // Proposed Resources Page JS Ends


    // View joiners pipeline JS Starts

    let months = []

    function getDaysInMonth(year, mon, m) {
        let month = [];
        let week = 1;
        let days = [];
        let monthNum = 0 + m;
        let thisMonth = '';


        switch (mon) {
            case 0: thisMonth = "January";
                break;
            case 1: thisMonth = "February";
                break;
            case 2: thisMonth = "March";
                break;
            case 3: thisMonth = "April";
                break;
            case 4: thisMonth = "May";
                break;
            case 5: thisMonth = "June";
                break;
            case 6: thisMonth = "July";
                break;
            case 7: thisMonth = "August";
                break;
            case 8: thisMonth = "September";
                break;
            case 9: thisMonth = "October";
                break;
            case 10: thisMonth = "November";
                break;
            case 11: thisMonth = "December";
                break;
        }

        let date = new Date(Date.UTC(year, mon));

        while (date.getMonth() === mon) {
            days.push(new Date(date));
            date.setDate(date.getDate() + 1);
        }

        month["week" + week] = []
        months['month' + monthNum] = []
        for (i = 0; i < days.length; i++) {

            if (days[i].getUTCDay() > 0 && days[i].getUTCDay() < 6) {
                let weekday = '';
                switch (days[i].getUTCDay()) {
                    case 1:
                        weekday = "Mon";
                        break;
                    case 2:
                        weekday = "Tue";
                        break;
                    case 3:
                        weekday = "Wed";
                        break;
                    case 4:
                        weekday = "Thu";
                        break;
                    case 5:
                        weekday = "Fri";
                }
                let date = '' + days[i].getUTCDate();
                let monthNum = '' + (days[i].getUTCMonth() + 1);
                if (date.length == 1) {
                    date = 0 + date;
                }
                if (monthNum.length == 1) {
                    monthNum = 0 + monthNum;
                }
                month["week" + week].push({
                    "date": date,
                    "week": weekday,
                    "month": monthNum,
                    "year": days[i].getUTCFullYear()
                })
            }
            if (days[i].getUTCDay() == 0 && i > 1 && i < days.length - 1) {
                week = week + 1;
                month["week" + week] = []
            }
            months['month' + monthNum] = { 'month': thisMonth + ' ' + year, 'info': month, 'formate': (mon + 1) + '/' + year };
        }

    }

    function getMonths() {
        let currentDate = new Date();
        let currentMonth = currentDate.getMonth();
        let currentYear = currentDate.getFullYear();;
        for (let i = 0; i < 3; i++) {

            if (i == 0) {
                currentMonth = currentMonth + 0;
            } else {
                currentMonth = currentMonth + 1;
            }


            if (currentMonth > 11) {
                currentMonth = currentMonth % 12;
                currentYear = currentYear + 1;
            }

            getDaysInMonth(currentYear, currentMonth, i);

        }
    }

    getMonths();

    for (i = 0; i < 3; i++) {

        let month = $("<div><h3 class='text-center month-name'>" + months['month' + i]['month'] + "</h3></div>").addClass('slick-child month text-center');
        let completeWeek = $('<div/>').addClass('completeWeek');

        for (j = 1; j <= 5; j++) {
            let week = $("<div><h4 class='text-center week-num'>" + ['week ' + j] + "</h4></div>").addClass('week');
            let allDays = $('<div/>').addClass('alldays');
            if (months['month' + i]['info']['week' + j] != undefined) {
                for (y = 0; y < months['month' + i]['info']['week' + j].length; y++) {
                    let aDay = $("<p class='each-day'><span class='date'>" + months['month' + i]['info']['week' + j][y]['date'] + "</span><span class='weekday'>" + months['month' + i]['info']['week' + j][y]['week'] + "</span>").attr('date-info', months['month' + i]['info']['week' + j][y]['date'] + '/' + months['month' + i]['info']['week' + j][y]['month'] + '/' + months['month' + i]['info']['week' + j][y]['year'])
                    allDays.append(aDay)
                    allDays.appendTo(week);
                }
            } else {
                break;
            }

            completeWeek.append(week).appendTo(month);
        }

        month.appendTo('.view-joiners-pipeline-landing .calendar');

    }

    $('.view-joiners-pipeline-landing .calendar').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        infinite: false,
        draggable: false,
        swipe: false,
        swipeToSlide: false,
        touchMove: false,
    });

    if ($(window).width() < 1025) {


        $('.view-joiners-pipeline-landing .calendar .completeWeek').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            centerMode: false,
            infinite: false
        });
    }

    //nav tab click and active

    $('.view-joiners-pipeline-landing .navbar ul.navbar-nav li').on('click', function () {
        if ($(this).attr('data-name') != 'requisition-view') {
            $(this).parents('ul.nav').children('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            })
            $(this).addClass('active');
            $('.view-joiners-pipeline-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            });
            $('.view-joiners-pipeline-landing .tbl-cntr').find('#' + $(this).attr('data-name')).parents('.individual-tbl').addClass('active');
        }
    })


    $('.view-joiners-pipeline-landing #tech-stack').bootstrapTable({
        pagination: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.view-joiners-pipeline-landing #bu-stack').bootstrapTable({
        pagination: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.view-joiners-pipeline-landing #requisition-view').bootstrapTable({
        pagination: true,
        sortable: true,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    // on click req number go to req view

    viewJoinersNoReqClick();

    $('.view-joiners-pipeline-landing .calendar .month .completeWeek .week h4').on('click', function () {
        $(this).parents('.calendar').find('.week-num').each(function (index, week) {
            if ($(week).hasClass('active')) {
                $(week).removeClass('active')
            }
            $(week).siblings('.alldays').find('p').each(function (index, day) {
                if ($(day).hasClass('active')) {
                    $(day).removeClass('active')
                }
            })
        })
        let allDays = $(this).siblings('.alldays').find('p');
        let weekFirstDay;
        let weekLastDay;
        allDays.each(function (index, weekDay) {
            if (index == 0) {
                weekFirstDay = $(weekDay).attr('date-info');
            }
            if (index == (allDays.length - 1)) {
                weekLastDay = $(weekDay).attr('date-info');
            }
        })
        alert(weekFirstDay + ',' + weekLastDay);

        $(this).addClass('active');
    })

    $('.view-joiners-pipeline-landing .calendar .month .completeWeek .week .alldays .each-day').on('click', function () {
        $(this).parents('.calendar').find('.each-day').each(function (index, day) {
            if ($(day).hasClass('active')) {
                $(day).removeClass('active')
            }
        })

        $(this).parents('.calendar').find('.week-num').each(function (index, week) {
            if ($(week).hasClass('active')) {
                $(week).removeClass('active')
            }
        });
        $(this).parent('.alldays').siblings('.week-num').addClass('active');
        $(this).addClass('active');
        alert($(this).attr('date-info'))
    })

    $('.view-joiners-pipeline-landing .calendar .month .month-name').on('click', function () {
        $(this).parents('.calendar').find('.each-day').each(function (index, day) {
            if ($(day).hasClass('active')) {
                $(day).removeClass('active')
            }
        })
        $(this).parents('.calendar').find('.week-num').each(function (index, week) {
            if ($(week).hasClass('active')) {
                $(week).removeClass('active')
            }
        });

        let allDays = $(this).siblings('.completeWeek').find('.each-day');
        let monthFirstDay;
        let monthLastDay;

        allDays.each(function (index, monthDay) {
            if (index == 0) {
                monthFirstDay = $(monthDay).attr('date-info');
            }
            if (index == (allDays.length - 1)) {
                monthLastDay = $(monthDay).attr('date-info');
            }
        })

        $(this).addClass('active');

        alert(monthFirstDay + ',' + monthLastDay);

    })

    $('.view-joiners-pipeline-landing .calendar > .slick-next, .view-joiners-pipeline-landing .calendar > .slick-prev').on('click', function () {
        $('.calendar').find('.each-day').each(function (index, day) {
            if ($(day).hasClass('active')) {
                $(day).removeClass('active')
            }
        });
        $('.calendar').find('.week-num').each(function (index, week) {
            if ($(week).hasClass('active')) {
                $(week).removeClass('active')
            }
        });
        $('.calendar > .slick-list .slick-active').children('.month-name').addClass('active');
        let allDays = $('.calendar > .slick-list .slick-active').find('.each-day');
        let monthFirstDay;
        let monthLastDay;

        allDays.each(function (index, monthDay) {
            if (index == 0) {
                monthFirstDay = $(monthDay).attr('date-info');
            }
            if (index == (allDays.length - 1)) {
                monthLastDay = $(monthDay).attr('date-info');
            }
        })

        alert(monthFirstDay + ',' + monthLastDay);
    });

    // View joiners pipeline JS Ends

    // CEG page JS Starts


    //nav tab click and active

    function cegPageFocusOnOptions() {
        if ($(window).width() < 700) {
            $('.ceg-main-landing .navbar ul').find('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $('.ceg-main-landing .navbar ul').animate({ scrollLeft: ($(item).width() * index) }, 500);
                }
            })
        }
    }

    $('.ceg-main-landing .navbar ul.navbar-nav li').on('click', function () {
        $(this).parents('ul.nav').children('li').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        })
        $(this).addClass('active');
        cegPageFocusOnOptions();
        $('.ceg-main-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        });
        $('.ceg-main-landing .tbl-cntr').find('#' + $(this).attr('data-name')).parents('.individual-tbl').addClass('active');
    })

    cegPageFocusOnOptions();


    $('.ceg-main-landing #identified-resources').bootstrapTable({
        pagination: true,
        showColumns: true,
        showColumnsToggleAll: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.ceg-main-landing #available-roaster').bootstrapTable({
        pagination: true,
        showColumns: true,
        showColumnsToggleAll: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.ceg-main-landing #resigned-exited-cases').bootstrapTable({
        pagination: true,
        showColumns: true,
        showColumnsToggleAll: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.ceg-main-landing #overall-deployment').bootstrapTable({
        pagination: true,
        showColumns: true,
        showColumnsToggleAll: true,
        sortable: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    // CEG page JS Ends

    // approve page JS Starts 


    $('.approve-page-landing .tbl-cntr #re-provisional-allocation').bootstrapTable({
        pagination: true,
        sortable: true,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });


    $('.approve-page-landing .tbl-cntr #external-hiring').bootstrapTable({
        pagination: true,
        sortable: true,
        showColumns: true,
        showColumnsToggleAll: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, 'ALL']
    });

    $('.approve-page-landing .navbar ul.navbar-nav li').on('click', function () {
        if ($(this).attr('data-name') != 'requisition-view') {
            $(this).parents('ul.nav').children('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            })
            $(this).addClass('active');
            $('.approve-page-landing .tbl-cntr').children('.individual-tbl').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            });
            $('.approve-page-landing .tbl-cntr').find('#' + $(this).attr('data-name')).parents('.individual-tbl').addClass('active');
        }
    })
    // approve page JS Ends


    // common js code starts 

    $('.gry-bgnd .popup-box textarea').on('change keyup paste', function () {

        var value = $(this).val().replace(/[^A-Z0-9]/ig, "");
        if (value.length != 0 && $(this).siblings('.err-msg').hasClass('show')) {
            $(this).siblings('.err-msg').removeClass('show');
        }
    })

    $('.gry-bgnd .popup-box button').on('click', function () {

        var value = $(this).siblings('textarea').val().replace(/[^A-Z0-9]/ig, "");
        if (value === '' || value.length == 0) {
            $(this).siblings('.err-msg').addClass('show');
            return false;
        } else {
            $(popupInfo).hide();
            $(popupInfo).siblings('.edit').show();
            $(popupInfo).siblings('.cancel').hide();
            $(popupInfo).parents('td').removeClass('expnd');
            $(popupInfo).parents('tr').find('textarea, input').each(function (index, item) {
                $(item).attr('disabled', true);
            })
            $(popupInfo).parents('tr').find('select').each(function (index, item) {
                $(item).attr('disabled', true);
                $(item).addClass('hide');
                $(item).siblings('label').removeClass('hide');
            })
            $(this).parents('.gry-bgnd').removeClass('show');
        }
    })

    $('.gry-bgnd .popup-box .cls-btn').on('click', function () {
        $(this).parents('.gry-bgnd').removeClass('show');
    })


    $('.tbl-cntr tbody td span.data-info input[type=checkbox]').on('click', function () {
        $(this).parents('tr').toggleClass('selected');
    })

    viewMore()
    rowEdit();
    rowUpdate();
    rowCancel();

    $('.gry-bgnd .popup-box.details .btn').on('click', function () {
        $(this).parents('.gry-bgnd').removeClass('show');
    })
    // common js code ends 
})

document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
        //  document.getElementById('contents').style.visibility="hidden";
    } else if (state == 'complete') {
        setTimeout(function () {
            document.getElementById('preloader').style.display = "none";
        }, 1000);
    }
}

